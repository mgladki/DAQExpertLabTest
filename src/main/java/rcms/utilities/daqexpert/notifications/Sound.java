package rcms.utilities.daqexpert.notifications;

public enum Sound {

	DEFAULT,
	STATE_CHANGE_LHC_BEAM_MODE,
	STATE_CHANGE_LHC_MACHINE_MODE,
	STATE_CHANGE_DAQ,
	NEW_RUN,

	KNOWN,
	DEADTIME,
	COMPLETED,
	DARK_CHANGE,
	CROW,
	DROP;
}