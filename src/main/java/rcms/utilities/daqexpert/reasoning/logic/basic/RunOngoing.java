package rcms.utilities.daqexpert.reasoning.logic.basic;

import java.util.Map;

import rcms.utilities.daqaggregator.data.DAQ;
import rcms.utilities.daqaggregator.data.SubSystem;
import rcms.utilities.daqexpert.notifications.Sound;
import rcms.utilities.daqexpert.reasoning.base.SimpleLogicModule;
import rcms.utilities.daqexpert.reasoning.base.enums.EventGroup;
import rcms.utilities.daqexpert.reasoning.base.enums.EventPriority;

public class RunOngoing extends SimpleLogicModule {

	public RunOngoing() {
		this.name = "Run ongoing";
		this.group = EventGroup.RUN_ONGOING;
		this.priority = EventPriority.DEFAULTT;
		this.description = "Run is ongoing according to TCDS state";
		this.setNotificationPlay(true);
		this.setSoundToPlay(Sound.NEW_RUN);
		this.setSkipText(true);
	}

	@Override
	public boolean satisfied(DAQ daq, Map<String, Boolean> results) {

		for (SubSystem curr : daq.getSubSystems()) {

			/* check tcds subsystem state */
			if (curr.getName().equalsIgnoreCase("TCDS")) {// change to constant
				String tcdsStatus = curr.getStatus();

				/* tcds can be running, pausing, paused, hard resseting */
				if (tcdsStatus.equalsIgnoreCase("running") || tcdsStatus.equalsIgnoreCase("paused")
						|| tcdsStatus.equalsIgnoreCase("pausing") || tcdsStatus.equalsIgnoreCase("resuming")
						|| tcdsStatus.equalsIgnoreCase("TTCHardResettingFromRunning")
						|| tcdsStatus.equalsIgnoreCase("TTCHardResetting")
						|| tcdsStatus.equalsIgnoreCase("TTCResyncingFromRunning")) {
					String l0 = daq.getLevelZeroState();
					if (!l0.equalsIgnoreCase("stopping") && !l0.equalsIgnoreCase("halting")
							&& !l0.equalsIgnoreCase("forcestopping") && !l0.equalsIgnoreCase("forcehalting")
							&& !l0.equalsIgnoreCase("undefined"))
						return true;
				}
			}
		}
		return false;
	}
}
